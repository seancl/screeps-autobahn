module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "parserOptions": {
        "sourceType": "module"
    },
    "extends": "eslint:recommended",
    "rules": {
        "indent": [
            "error",
            "tab",
            {
                "SwitchCase": 1
            }
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-console": "off",
    },
    "globals": {
        "_": false,
        "Game": false,
        "RoomVisual": false,
        "RoomPosition": false,
        "ERR_INVALID_ARGS": false,
        "ERR_NO_PATH": false,
        "ERR_NOT_FOUND": false,
        "STRUCTURE_ROAD": false,
        "STRUCTURE_CONTAINER": false,
        "STRUCTURE_RAMPART": false,
        "LOOK_STRUCTURES": false,
        "TERRAIN_MASK_WALL": false,
    }
};

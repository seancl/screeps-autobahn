import resolve from 'rollup-plugin-node-resolve';

export default [
	{
		input: 'src/autobahn.default.js',
		output: {
			file: 'dist/autobahn.js',
			format: 'cjs'
		},
		plugins: [
			resolve(),
		],
	},
	{
		input: 'src/index.js',
		output: {
			file: 'dist/autobahn.es.js',
			format: 'es'
		},
		plugins: [
			resolve(),
		],
	},
];

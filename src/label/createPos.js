export default function createPos(x, y, roomName) {
	switch (typeof x) {
		case 'object':
			return {x: x.x, y: x.y, roomName: x.roomName};
		case 'string':
			return {x: parseInt(x), y: parseInt(y), roomName: roomName};
		default:
			return {x: x, y: y, roomName: roomName};
	}
}

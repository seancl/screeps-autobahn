import createPos from './createPos';

export default function getPos(label) {
	return createPos(...label.split(','));
}

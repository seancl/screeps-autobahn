import createLabel from './createLabel';

function isRoomAllowed(roomName, roomFilter) {
	if (!roomName) {
		return false;
	}

	switch (typeof roomFilter) {
		case 'function':
			return roomFilter(roomName);
		case 'object':
			return _.includes(roomFilter, roomName);
		case 'string':
			return roomFilter === roomName;
		default:
			return false;
	}
}

export default function smartCreateLabel(x, y, origRoomName, roomFilter) {
	let dir = 'none';

	if (x < 0) {
		x = 48;
		dir = '7';
	}
	else if (x > 49) {
		x = 1;
		dir = '3';
	}
	else if (y < 0) {
		y = 48;
		dir = '1';
	}
	else if (y > 49) {
		y = 1;
		dir = '5';
	}

	let roomName = dir === 'none' ? origRoomName : _.get(Game.map.describeExits(origRoomName), dir);

	if (isRoomAllowed(roomName, roomFilter)) {
		return createLabel(x, y, roomName);
	}

	return null;
}

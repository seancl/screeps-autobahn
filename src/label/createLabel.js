export default function createLabel(x, y, roomName) {
	return `${x},${y},${roomName}`;
}

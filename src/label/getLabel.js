export default function getLabel(pos) {
	return `${pos.x},${pos.y},${pos.roomName}`;
}

export default function stepGraph(fromLabel, graphs) {
	let distance = graphs.distGraph[fromLabel];
	let stepGraph = [];
	stepGraph[distance] = [fromLabel];
	for (let i = distance - 1; i >= 0; i--) {
		let nextSteps = _.map(stepGraph[i+1], label => graphs.parentGraph[label]);
		stepGraph[i] = _.union(...nextSteps);
	}

	return stepGraph;
}

export default function roomPosition(label) {
	if (typeof label !== 'string') {
		return ERR_INVALID_ARGS;
	}

	let [x, y, roomName] = label.split(',');
	return new RoomPosition(x, y, roomName);
}

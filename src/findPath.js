export default function findPath(fromLabel, toLabel, graphs) {
	let graph = graphs.getStepGraph(fromLabel);
	let dist = graphs.distGraph[toLabel];
	let max = graphs.distGraph[fromLabel];
	let label = toLabel;
	let path = [toLabel];

	while (fromLabel !== label && dist < max) {
		dist++;
		label = _.find(graph[dist], child => _.includes(graphs.parentGraph[child], label));
		path.push(label);
	}

	return path;
}

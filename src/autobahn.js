import objectLabel from './objectLabel';
import buildGraphs from './buildGraphs';
import bestNetworks from './bestNetworks';
import materializeNetwork from './materializeNetwork';
import roomPosition from './roomPosition';

/**
 * Finds a road network connecting the start position to the destinations.
 * @param start {RoomPosition}: The start position of the network.
 * @param destinations {RoomPosition[]}: Destinations for the network to reach.
 * @param options {object}: Not yet implemented.
 * @returns {RoomPosition[]}
 */

export default function autobahn(start, destinations, options = {}) {
	let startLabel = objectLabel(start);
	let destLabels = _.map(destinations, objectLabel);

	if (startLabel === 'error' || _.includes(destLabels, 'error')) {
		return ERR_INVALID_ARGS;
	}

	let graphs = buildGraphs(startLabel, destLabels, options);

	if (graphs === ERR_NO_PATH) {
		return ERR_NO_PATH;
	}

	let {networks} = bestNetworks(startLabel, destLabels, graphs, options);

	if (networks.length === 0) {
		return ERR_NOT_FOUND;
	}

	let positions = materializeNetwork(_.first(networks), graphs, options);
	let output = _.map(positions, roomPosition);

	return output;
}

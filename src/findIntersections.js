export default function findIntersections(labels, graphs) {
	let distances = _.map(labels, label => graphs.distGraph[label]);
	let stepGraphs = _.map(labels, label => graphs.getStepGraph(label));

	let dist = _.min(distances);
	let intersections = [];

	while (intersections.length === 0) {
		let labelSets = _.map(stepGraphs, graph => graph[dist]);
		intersections = _.intersection(...labelSets);
		dist--;
	}

	return intersections;
}

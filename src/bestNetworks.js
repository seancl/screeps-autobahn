function pairs(array) {
	let len = array.length;
	let result = [];
	for (let i = 0; i < len; i++) {
		for (let j = i + 1; j < len; j++) {
			result.push([array[i], array[j]]);
		}
	}

	return result;
}

function getJunctions(pair, graphs) {
	let intersections = graphs.getIntersections(pair);
	return _.map(intersections, label => [...pair, label]);
}

function updateDestinations(junction, destinations) {
	let cleanedDestinations = _.without(destinations, ...junction);
	return _.sortBy([...cleanedDestinations, _.last(junction)]);
}

function recursiveSearch(start, destinations, graphs, path = []) {
	if (destinations.length === 1) {
		return [[...path, [...destinations, start]]];
	}
	let destPairs = pairs(destinations);
	let junctions = _.flatten(_.map(destPairs, pair => getJunctions(pair, graphs)));

	return _.flatten(_.map(junctions, junction =>
		recursiveSearch(
			start,
			updateDestinations(junction, destinations),
			graphs,
			[...path, junction]
		)
	));
}

function junctionCost(junction, graphs) {
	// junction is an array of labels which all converge
	// on the final label in the array

	let dists = _.map(junction, label => graphs.distGraph[label]);
	// use intersection dist, plus one to exclude the cost
	// of the intersection level from each inbound segment
	let endDist = dists.pop() + 1;
	// add back the cost of the intersection itself ONCE
	return _.sum(dists, dist => dist - endDist) + 1;
}

function getBest(result, network, costFunction) {
	// network is an array of junction arrays.
	// each junction array is an array of labels which
	// all converge on the final label in the array.
	let cost = _.sum(network, costFunction);
	if (cost < result.cost) {
		result.cost = cost;
		result.networks = [network];
	} else if (cost === result.cost) {
		result.networks.push(network);
	}

	return result;
}

export default function bestNetworks(start, destinations, graphs) {
	let arr = _.sortBy(destinations);
	let allNetworks = recursiveSearch(start, arr, graphs);
	let curriedCost = junction => junctionCost(junction, graphs);
	let curriedBest = (result, network) => getBest(result, network, curriedCost);

	return _.reduce(allNetworks, curriedBest, {cost: Infinity, networks: []});
}

export default function objectLabel(gameObject) {
	if (typeof gameObject !== 'object') {
		return 'error';
	}

	let pos = gameObject;
	if (!(gameObject instanceof RoomPosition)) {
		pos = _.get(gameObject, 'pos');
	}

	if (!(pos instanceof RoomPosition)) {
		return 'error';
	}

	return `${pos.x},${pos.y},${pos.roomName}`;
}

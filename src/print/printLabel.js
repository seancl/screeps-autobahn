import getPos from '../label/getPos';

export default function printLabel(label, circleOptions) {
	let pos = getPos(label);
	new RoomVisual(pos.roomName).circle(pos, circleOptions);
}

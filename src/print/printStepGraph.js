import getPos from '../label/getPos';

export default function printStepGraph(stepGraph, circleOptions) {
	_.forEach(_.flattenDeep(stepGraph), label => {
		let pos = getPos(label);
		new RoomVisual(pos.roomName).circle(pos, circleOptions);
	});
}

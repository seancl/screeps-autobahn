import { getPos } from '../label/index';

export default function printGraph(parentGraph) {
	_.forEach(parentGraph, (parents, label) =>
		_.forEach(parents, parent =>
			new RoomVisual('sim').line(getPos(label), getPos(parent))
		)
	);
}

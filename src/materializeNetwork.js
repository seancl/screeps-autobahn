import findPath from './findPath';

function materializeJunction(junction, graphs) {
	let feeders = _.initial(junction);
	let intersection = _.last(junction);

	let paths = _.map(feeders, feeder =>
		// strip final label from path because it's the intersection
		_.initial(findPath(feeder, intersection, graphs))
	);
	// add back the intersection once
	return [..._.flatten(paths), intersection];
}

export default function materializeNetwork(network, graphs) {
	let curriedMatJunct = junct => materializeJunction(junct, graphs);
	return _.union(..._.map(network, curriedMatJunct));
}

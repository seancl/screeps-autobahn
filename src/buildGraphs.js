import * as l from './label';
import stepGraph from './stepGraph';
import findIntersections from './findIntersections';


function crawlNeighbors(label, graph, destinations, roomFilter) {
	let result = {destsFound: [], newLabels: [], validSteps: []};
	let pos = l.getPos(label);
	for (let dx = -1; dx <= 1; dx++) {
		for (let dy = -1; dy <= 1; dy++) {
			let x = pos.x + dx;
			let y = pos.y + dy;
			let newLabel = l.smartCreateLabel(x, y, pos.roomName, roomFilter);

			if (!newLabel) {
				continue;
			}

			if (destinations.has(newLabel)) {
				result.destsFound.push(newLabel);
			}

			let dist = graph[newLabel];
			if (dist === undefined) {
				result.newLabels.push(newLabel);
			} else if (dist > graph[label]) {
				result.validSteps.push(newLabel);
			}
		}
	}

	return result;
}

function attachGetters(object) {
	let getStepGraph = (fromLabel) => stepGraph(fromLabel, object);
	object.getStepGraph = _.memoize(getStepGraph);
	let getIntersections = (labels) => findIntersections(labels, object);
	object.getIntersections = _.memoize(getIntersections);

	return object;
}

function isBlocking(structure) {
	return (
		structure.structureType !== STRUCTURE_ROAD
		&& structure.structureType !== STRUCTURE_CONTAINER
		&& !(structure.structureType === STRUCTURE_RAMPART && structure.my)
	);
}

function isWalkable(label) {
	let pos = l.getPos(label);
	let room = Game.rooms[pos.roomName];
	let walkable = Game.map.getRoomTerrain(pos.roomName).get(pos.x, pos.y) !== TERRAIN_MASK_WALL;

	if (walkable && room) {
		let structs = room.lookForAt(LOOK_STRUCTURES, pos.x, pos.y);
		walkable = (_.find(structs, isBlocking) === undefined);
	}

	return walkable;
}

/**
 * Builds a directed acyclic graph around the start to the destinations.
 * @param start {string}: The start position label of the network.
 * @param destinations {string[]}: Destination position labels.
 * @param options {object}: Not yet implemented.
 * @returns {Object} An object containing parentGraph and distGraph
 */
export default function buildGraphs(start, destinations, options) {
	let parentGraph = Object.create(null);
	parentGraph[start] = [];
	let distGraph = Object.create(null);
	distGraph[start] = 0;
	let queue = [];
	let nextQueue = [start];
	let distance = 0;
	let remainingDests = new Set(destinations);
	let roomFilter = _.get(options, 'roomFilter', l.getPos(start).roomName);

	while (remainingDests.size > 0) {
		queue = nextQueue;
		nextQueue = [];
		distance++;

		if (queue.length === 0) {
			return ERR_NO_PATH;
		}

		while (queue.length > 0) {
			let label = queue.pop();
			let results = crawlNeighbors(label, distGraph, remainingDests, roomFilter);
			_.forEach(results.newLabels, newLabel => {
				if (isWalkable(newLabel)) {
					nextQueue.push(newLabel);
				}
				distGraph[newLabel] = distance;
				parentGraph[newLabel] = [label];
			});
			_.forEach(results.validSteps, step => parentGraph[step].push(label));
			_.forEach(results.destsFound, dest => remainingDests.delete(dest));
		}
	}

	return attachGetters({parentGraph, distGraph});
}

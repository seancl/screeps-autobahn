import autobahnFunc from './autobahn';

import * as index from './index';

for (let item in index) {
	autobahnFunc[item] = index[item];
}

export default autobahnFunc;
